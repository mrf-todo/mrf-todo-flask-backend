from flask import Flask, jsonify, request
from flask_cors import CORS
import uuid


TODOS = [
    {
        "id": uuid.uuid4().hex,
        "text": "Flask Backend",
        "done": True,
    },
    {
        "id": uuid.uuid4().hex,
        "text": "Vue Frontend",
        "done": False,
    },
    {
        "id": uuid.uuid4().hex,
        "text": "Schwimmen gehen",
        "done": False,
    }
]

DEBUG = True

app = Flask(__name__)
app.config.from_object(__name__)


CORS(app)


@app.route("/todos", methods=["GET", "POST"])
def all_todos():
    response_object = {"status": "success"}
    if request.method == "POST":
        post_data = request.get_json()
        TODOS.insert(0, {
            "id": uuid.uuid4().hex,
            "text": post_data.get("text"),
            "done": post_data.get("done"),
        })
        move_done()
        response_object["message"] = "Todo hinzugefügt!"
    else:
        move_done()
        response_object["todos"] = TODOS
    return jsonify(response_object)


@app.route("/todos/toggle-done/<todo_id>", methods=["PUT"])
def toggle_todo(todo_id):
    response_object = {"status": "success"}
    if request.method == "PUT":
        toggle_todo_done(todo_id)
        move_done()
        response_object["message"] = "Todo getoggelt!"
    return jsonify(response_object)

def toggle_todo_done(todo_id):
    for todo in TODOS:
        if todo["id"] == todo_id:
            if todo["done"]:
                todo["done"] = False
            else:
                todo["done"] = True
            return True
    return False

def move_done():
    for todo in TODOS:
        if todo["done"]:
            TODOS.remove(todo)
            TODOS.append(todo)


@app.route("/todos/remove/<todo_id>", methods=["DELETE"])
def remove_todo(todo_id):
    response_object = {"status": "success"}
    if request.method == "DELETE":
        remove_todo_help(todo_id)
        response_object["message"] = "Todo gelöscht!"
    return jsonify(response_object)

def remove_todo_help(todo_id):
    for todo in TODOS:
        if todo["id"] == todo_id:
            TODOS.remove(todo)
            return True
    return False


@app.route("/todos/edit/<todo_id>", methods=["PUT"])
def edit_todo(todo_id):
    response_object = {"status": "success"}
    if request.method == "PUT":
        post_data = request.get_json()
        remove_todo_help(todo_id)
        TODOS.insert(0, {
            "id": uuid.uuid4().hex,
            "text": post_data.get("text"),
            "done": post_data.get("done"),
        })
        move_done()
        response_object["message"] = "Todo updated!"
    return jsonify(response_object)


if __name__ == "__main__":
    app.run()
